package main

import (
	"fmt"
	"time"
)

func main() {
	for i := 10; i >= 10; i++ {
		timer1 := time.NewTimer(300 * time.Second)

		<-timer1.C
		fmt.Printf("Hit API Progress,%v", time.Now())
		fmt.Println()

	}

}
