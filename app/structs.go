package app

type LorealFeedResponse struct {
	Data []struct {
		Name    string `json:"name"`
		Message string `json:"message"`
	} `json:"data"`
}
