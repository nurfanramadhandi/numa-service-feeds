package app

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"qasircore"

	"github.com/parnurzeal/gorequest"
)

/**
 * setup httpclient services
 */
type HttpClient struct {
	Http     *gorequest.SuperAgent
	BaseUrl  string
	Endpoint string
	Service  string
	Method   string
	Headers  map[string]string
	Data     map[string]interface{}
}

/**
 * Set What header should send during http request to another services
 * @param key string
 * @param value string
 * @return void
 */
func (hc *HttpClient) SetHeader(key, value string) {
	// hc.Http.Set(key, value)
	if hc.Headers == nil {
		hc.Headers = map[string]string{
			key: value,
		}
	} else {
		hc.Headers[key] = value
	}
}

/**
 * Set Header, but with multiple header input
 * @param dataheaders map[string]interface{}
 * @return void
 */
func (hc *HttpClient) SetMultipleHeader(dataHeaders map[string]interface{}) {
	for key, val := range dataHeaders {
		hc.SetHeader(key, fmt.Sprint(val))
	}
}

/**
 * Send Request data for http request
 * @param data map[string]interface{}
 * @return void
 */
func (hc *HttpClient) SetData(data map[string]interface{}) {
	hc.Data = data
}

/**
 * Calling send function means that will send hit request with all the setup been configured
 * @return resp *http.Response
 */
func (hc *HttpClient) Send() *http.Response {
	fullUrl := hc.BaseUrl + hc.Endpoint + hc.Service

	request := hc.Http
	if hc.Method == gorequest.POST {
		bytes, _ := json.Marshal(&hc.Data)
		contentData := string(bytes)
		request = request.Post(fullUrl).Send(contentData)
	} else if hc.Method == gorequest.GET {
		urlEncodeData := qasircore.UrlEncodedString(hc.Data)
		request = request.Get(fullUrl + urlEncodeData)
	}

	for key, val := range hc.Headers {
		request = request.Set(key, val)
	}

	resp, _, err := request.End()

	if err != nil {
		log.Fatal(err)
		return resp
	} else {
		return resp
	}
}
