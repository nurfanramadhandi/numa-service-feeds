package app

import (
	"encoding/json"

	"github.com/parnurzeal/gorequest"
)

type Service struct {
	Curl      *Service
	Headers   map[string]string
	Subdomain string
	Endpoint  string
	Path      string
	Method    string
	Body      map[string]interface{}
	Param     map[string]interface{}
	Token     *string
}

func (s *Service) GetToken() string {
	if s.Token == nil {
		return ""
	}
	stringToken := *s.Token
	return stringToken
}

func (s *Service) SetHeader(key, value string) {
	if s.Headers == nil {
		s.Headers = map[string]string{
			key: value,
		}
	} else {
		s.Headers[key] = value
	}
}

func (s *Service) SetToken(token string) {
	s.Token = &token
	s.SetHeader("Authorization", "Bearer "+token)
}

func (s *Service) SendRequest() string {
	tokenString := s.Curl.GetToken()

	if tokenString != "" {
		s.SetHeader("Authorization", "Bearer "+tokenString)
	}

	var httpAgent *gorequest.SuperAgent

	if s.Method == "GET" || s.Method == "" {
		httpAgent = gorequest.New().Get(s.Endpoint + s.Path)
	} else if s.Method == "POST" {
		bytes, _ := json.Marshal(s.Body)
		httpAgent = gorequest.New().Post(s.Endpoint + s.Path)
		for key, val := range s.Headers {
			httpAgent = httpAgent.Set(key, val)
		}
		httpAgent = httpAgent.Send(string(bytes))
	}

	_, Body, _ := httpAgent.End()
	return Body
}

func NewService() *Service {
	var service Service

	service.Headers = map[string]string{
		"app-key": Env.Get("api_key"),
	}

	return &service
}
