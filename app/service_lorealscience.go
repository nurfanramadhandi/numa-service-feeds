package app

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
)

type LorealService struct {
}

func LorealFeeds() LorealFeedResponse {
	var result LorealFeedResponse

	uri := "https://www.lorealscience.id/api/feeds"

	response, err := http.Get(uri)
	if err != nil {
		fmt.Printf("%s", err)
		os.Exit(1)
	} else {
		defer response.Body.Close()
		contents, err := ioutil.ReadAll(response.Body)
		if err != nil {
			fmt.Printf("%s", err)
			os.Exit(1)
		}

		err = json.Unmarshal([]byte(contents), &result)

		if err != nil {
			panic(err)
		}
		fmt.Printf("%s\n", result)
		return result
	}

	return result
}
