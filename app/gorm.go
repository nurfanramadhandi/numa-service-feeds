package app

type ListPurchases struct {
	ID      int    `gorm:"column:id" json:"id"`
	Name    string `gorm:"column:name" json:"name"`
	Message string `gorm:"column:message" json:"message"`
}
